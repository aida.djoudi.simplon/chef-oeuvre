jQuery(document).ready(function() {
                var $collectionHolder;
                // setup an "add a tag" link
                var $addTagButton = $('<button type="button" class="btn btnColor"><i class="fas fa-male"></i> Ajouter un parent</button>');
                var $newLinkLi = $('<li></li>').append($addTagButton);
                
                // Get the ul that holds the collection of tags
                //changer par stephane
                $collectionHolder = $('#child1_parent');
                /*console.log($collectionHolder);
                if($collectionHolder === null || $collectionHolder === undefined)
                $collectionHolder = $('ul.parent') */
                // add the "add a tag" anchor and li to the tags ul
                $collectionHolder.append($addTagButton);

                // count the current form inputs we have (e.g. 2), use that as the new
                // index when inserting a new item (e.g. 2)
                $collectionHolder.data('index', $collectionHolder.find('input').length);

                $addTagButton.on('click', function(e) {
                    // add a new tag form (see next code block)
                    addTagForm($collectionHolder, $addTagButton);
                });
            });

            function addTagForm($collectionHolder, $newLinkLi) {
                // Get the data-prototype explained earlier
                //ajouter par stephane
                $proto = $('ul.parent')
                var prototype = $proto.data('prototype');

                // get the new index
                var index = $collectionHolder.data('index');

                var newForm = prototype;
                // You need this only if you didn't set 'label' => false in your tags field in TaskType
                // Replace '__name__label__' in the prototype's HTML to
                // instead be a number based on how many items we have
                // newForm = newForm.replace(/__name__label__/g, index);

                // Replace '__name__' in the prototype's HTML to
                // instead be a number based on how many items we have
               // index = 20;
                newForm = newForm.replace(/__name__/g, index);

                // increase the index with one for the next item
                $collectionHolder.data('index', index + 1);

                // Display the form in the page in an li, before the "Add a tag" link li
                var $newFormLi = $('<p></p>').append(newForm);
                $newLinkLi.before($newFormLi);
            }


