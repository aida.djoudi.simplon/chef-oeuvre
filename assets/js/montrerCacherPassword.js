// Fonction voir mot de passe
$(document).ready(function(){
    $('.show-password').click(function(){
        if ($(this).prev('input').prop('type')== 'password'){
            $(this).prev('input').prop('type','text');
            $(this).children('i').attr("class","fas fa-eye-slash");
        }else{
            $(this).prev('input').prop('type','password');
            $(this).children('i').attr("class","fas fa-eye");
        }
    });
});