/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';
import '../js/ajouterparent';
import '../js/supprimerparent';
import '../js/carto';
import '../js/buttonSelectionner.js';
import '../js/montrerCacherPassword.js';
import '../js/iconeGeolocalisation.js';
//import '../js/readMore.js';
// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';
import 'bootstrap';

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');
