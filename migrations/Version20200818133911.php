<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200818133911 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE school_team ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE school_team ADD CONSTRAINT FK_85C879F2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_85C879F2A76ED395 ON school_team (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE school_team DROP FOREIGN KEY FK_85C879F2A76ED395');
        $this->addSql('DROP INDEX UNIQ_85C879F2A76ED395 ON school_team');
        $this->addSql('ALTER TABLE school_team DROP user_id');
    }
}
