<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820120640 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE school_team_class_school (school_team_id INT NOT NULL, class_school_id INT NOT NULL, INDEX IDX_B4FEA267BB43EA36 (school_team_id), INDEX IDX_B4FEA267A8F5D8D1 (class_school_id), PRIMARY KEY(school_team_id, class_school_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE school_team_class_school ADD CONSTRAINT FK_B4FEA267BB43EA36 FOREIGN KEY (school_team_id) REFERENCES school_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_team_class_school ADD CONSTRAINT FK_B4FEA267A8F5D8D1 FOREIGN KEY (class_school_id) REFERENCES class_school (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE parent_child');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE parent_child (id INT AUTO_INCREMENT NOT NULL, parent_id INT NOT NULL, child_id INT NOT NULL, INDEX IDX_EE82C08A727ACA70 (parent_id), INDEX IDX_EE82C08ADD62C21B (child_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08A727ACA70 FOREIGN KEY (parent_id) REFERENCES parent_of_child (id)');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08ADD62C21B FOREIGN KEY (child_id) REFERENCES child (id)');
        $this->addSql('DROP TABLE school_team_class_school');
    }
}
