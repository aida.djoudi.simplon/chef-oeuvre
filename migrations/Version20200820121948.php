<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820121948 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE school_year_class_school (school_year_id INT NOT NULL, class_school_id INT NOT NULL, INDEX IDX_966689EFD2EECC3F (school_year_id), INDEX IDX_966689EFA8F5D8D1 (class_school_id), PRIMARY KEY(school_year_id, class_school_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school_year_evenement (school_year_id INT NOT NULL, evenement_id INT NOT NULL, INDEX IDX_F79A39A8D2EECC3F (school_year_id), INDEX IDX_F79A39A8FD02F13 (evenement_id), PRIMARY KEY(school_year_id, evenement_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE school_year_class_school ADD CONSTRAINT FK_966689EFD2EECC3F FOREIGN KEY (school_year_id) REFERENCES school_year (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_year_class_school ADD CONSTRAINT FK_966689EFA8F5D8D1 FOREIGN KEY (class_school_id) REFERENCES class_school (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_year_evenement ADD CONSTRAINT FK_F79A39A8D2EECC3F FOREIGN KEY (school_year_id) REFERENCES school_year (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_year_evenement ADD CONSTRAINT FK_F79A39A8FD02F13 FOREIGN KEY (evenement_id) REFERENCES evenement (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE parent_child');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE parent_child (id INT AUTO_INCREMENT NOT NULL, parent_id INT NOT NULL, child_id INT NOT NULL, INDEX IDX_EE82C08ADD62C21B (child_id), INDEX IDX_EE82C08A727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08A727ACA70 FOREIGN KEY (parent_id) REFERENCES parent_of_child (id)');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08ADD62C21B FOREIGN KEY (child_id) REFERENCES child (id)');
        $this->addSql('DROP TABLE school_year_class_school');
        $this->addSql('DROP TABLE school_year_evenement');
    }
}
