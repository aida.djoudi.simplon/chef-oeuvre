<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820114824 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE child_service (child_id INT NOT NULL, service_id INT NOT NULL, INDEX IDX_8329D2C9DD62C21B (child_id), INDEX IDX_8329D2C9ED5CA9E6 (service_id), PRIMARY KEY(child_id, service_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE child_service ADD CONSTRAINT FK_8329D2C9DD62C21B FOREIGN KEY (child_id) REFERENCES child (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE child_service ADD CONSTRAINT FK_8329D2C9ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE child DROP cafeteria, DROP nursery');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE child_service');
        $this->addSql('ALTER TABLE child ADD cafeteria TINYINT(1) NOT NULL, ADD nursery TINYINT(1) NOT NULL');
    }
}
