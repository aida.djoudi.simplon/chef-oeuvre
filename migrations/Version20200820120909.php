<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820120909 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE school_team_service (school_team_id INT NOT NULL, service_id INT NOT NULL, INDEX IDX_D5D65E5BBB43EA36 (school_team_id), INDEX IDX_D5D65E5BED5CA9E6 (service_id), PRIMARY KEY(school_team_id, service_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE school_team_service ADD CONSTRAINT FK_D5D65E5BBB43EA36 FOREIGN KEY (school_team_id) REFERENCES school_team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_team_service ADD CONSTRAINT FK_D5D65E5BED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE parent_child');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE parent_child (id INT AUTO_INCREMENT NOT NULL, parent_id INT NOT NULL, child_id INT NOT NULL, INDEX IDX_EE82C08ADD62C21B (child_id), INDEX IDX_EE82C08A727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08A727ACA70 FOREIGN KEY (parent_id) REFERENCES parent_of_child (id)');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08ADD62C21B FOREIGN KEY (child_id) REFERENCES child (id)');
        $this->addSql('DROP TABLE school_team_service');
    }
}
