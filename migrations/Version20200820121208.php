<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820121208 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE service_school_year (service_id INT NOT NULL, school_year_id INT NOT NULL, INDEX IDX_36308A76ED5CA9E6 (service_id), INDEX IDX_36308A76D2EECC3F (school_year_id), PRIMARY KEY(service_id, school_year_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE service_school_year ADD CONSTRAINT FK_36308A76ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE service_school_year ADD CONSTRAINT FK_36308A76D2EECC3F FOREIGN KEY (school_year_id) REFERENCES school_year (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE parent_child');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE parent_child (id INT AUTO_INCREMENT NOT NULL, parent_id INT NOT NULL, child_id INT NOT NULL, INDEX IDX_EE82C08A727ACA70 (parent_id), INDEX IDX_EE82C08ADD62C21B (child_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08A727ACA70 FOREIGN KEY (parent_id) REFERENCES parent_of_child (id)');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08ADD62C21B FOREIGN KEY (child_id) REFERENCES child (id)');
        $this->addSql('DROP TABLE service_school_year');
    }
}
