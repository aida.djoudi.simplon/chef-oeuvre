<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820115647 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE child_parent_of_child (child_id INT NOT NULL, parent_of_child_id INT NOT NULL, INDEX IDX_3697C24BDD62C21B (child_id), INDEX IDX_3697C24BAD52680 (parent_of_child_id), PRIMARY KEY(child_id, parent_of_child_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE child_parent_of_child ADD CONSTRAINT FK_3697C24BDD62C21B FOREIGN KEY (child_id) REFERENCES child (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE child_parent_of_child ADD CONSTRAINT FK_3697C24BAD52680 FOREIGN KEY (parent_of_child_id) REFERENCES parent_of_child (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE parent_child');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE parent_child (id INT AUTO_INCREMENT NOT NULL, parent_id INT NOT NULL, child_id INT NOT NULL, INDEX IDX_EE82C08A727ACA70 (parent_id), INDEX IDX_EE82C08ADD62C21B (child_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08A727ACA70 FOREIGN KEY (parent_id) REFERENCES parent_of_child (id)');
        $this->addSql('ALTER TABLE parent_child ADD CONSTRAINT FK_EE82C08ADD62C21B FOREIGN KEY (child_id) REFERENCES child (id)');
        $this->addSql('DROP TABLE child_parent_of_child');
    }
}
