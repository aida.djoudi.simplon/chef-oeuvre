<?php

namespace App\Repository;

use App\Entity\ParentOfChild;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParentOfChild|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParentOfChild|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParentOfChild[]    findAll()
 * @method ParentOfChild[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParentOfChildRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParentOfChild::class);
    }

    // /**
    //  * @return ParentOfChild[] Returns an array of ParentOfChild objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParentOfChild
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findParent($childsFId)
    {
        $qb = $this
        ->createQueryBuilder('p')
        ->Join('p.children', 'c')
        // ->where('s.id = c.id')
        // ->andWhere('s.id= :val')
        // ->setParameter('val', $idService)
        ->where('c.id = :id')
        ->setParameter('id',$childsFId);
        $query = $qb->getQuery();
        $results = $query->getResult();
        return $results;
        ;
    }
}
