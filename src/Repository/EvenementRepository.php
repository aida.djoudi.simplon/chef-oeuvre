<?php

namespace App\Repository;

use App\Entity\Evenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Evenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evenement[]    findAll()
 * @method Evenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evenement::class);
    }

    // /**
    //  * @return Evenement[] Returns an array of Evenement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Evenement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findPublicEvent($value)
    {
        return $this->createQueryBuilder('e')
        ->andWhere('e.type LIKE :param')
        ->setParameter("param","%{$value}%")
        ->orderBy('e.id','DESC')
        ->getQuery()
        ->getResult()
        
        ;
    
    }
    public function findLastEventWithvalue($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.type LIKE :param')
            ->setParameter("param","%{$value}%")
            ->orderBy('e.id','DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }
    //chercher evenement avec type choisi dans controleur et envoyer email a seulement si type evenement choisi et le meme
    public function findLastEvent()
    {
        return $this->createQueryBuilder('e')
            ->orderBy('e.id','DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }
    public function findEventWithvalue($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.type LIKE :param')
            ->setParameter("param","%{$value}%")
            ->orderBy('e.id','DESC')
            ->getQuery()
            ->getResult()
        ;
    }
    
}
