<?php

namespace App\Repository;

use App\Entity\SchoolTeam;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SchoolTeam|null find($id, $lockMode = null, $lockVersion = null)
 * @method SchoolTeam|null findOneBy(array $criteria, array $orderBy = null)
 * @method SchoolTeam[]    findAll()
 * @method SchoolTeam[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolTeamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SchoolTeam::class);
    }

    // /**
    //  * @return SchoolTeam[] Returns an array of SchoolTeam objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SchoolTeam
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    
    // public function findTeacher($val1,$val2)
    // {
    //     return $this->createQueryBuilder('t')
    //         ->Where('t.firstName LIKE :param')
    //         ->Where('t.lastName LIKE :param')
    //         ->setParameter("param","%{$val1}%")
    //         ->setParameter("param","%{$val2}%")
    //         ->getQuery()
    //         ->getResult()
    //     ;
    // }
    public function findTeacher($val)
    {
        $qb = $this
        ->createQueryBuilder('t')
        ->Join('t.user', 'user')
        ->where('user.id = :id')
        ->setParameter('id', $val);
        $query = $qb->getQuery();
        $results = $query->getResult();
        return $results;
        ;
    }
}
