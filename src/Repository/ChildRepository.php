<?php

namespace App\Repository;

use App\Entity\Child;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Child|null find($id, $lockMode = null, $lockVersion = null)
 * @method Child|null findOneBy(array $criteria, array $orderBy = null)
 * @method Child[]    findAll()
 * @method Child[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChildRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Child::class);
    }

    //une methode pour choper les enfant qui sont inscrit au service
    public function findChildWithService($idService)
    {
        $qb = $this
        ->createQueryBuilder('c')
        ->innerJoin('c.service', 's')
        ->where('s.id = :id')
        ->setParameter('id', $idService);
        $query = $qb->getQuery();
        $results = $query->getResult();
        return $results;
        ;
    }
     //une methode pour choper les enfant qui sont inscrit au service
    public function findChildWithClasse($idClasse)
        {
            $qb = $this
            ->createQueryBuilder('c')
            ->Join('c.class', 'classe')
            // ->where('s.id = c.id')
            // ->andWhere('s.id= :val')
            // ->setParameter('val', $idService)
            ->where('classe.id = :id')
            ->setParameter('id', $idClasse);
            $query = $qb->getQuery();
            $results = $query->getResult();
            return $results;
            ;
        }

}
