<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class EvenementVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['edit','view','delete'])
            && $subject instanceof \App\Entity\Evenement;
    }

    protected function voteOnAttribute($attribute, $event, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'edit':
                return $event->getUser()==$user;
                // logic to determine if the user can EDIT
                // return true or false
                break;
            case 'delete':
                return $event->getUser()==$user;
                // logic to determine if the user can VIEW
                // return true or false
                break;
            case 'view':
                return $event->getUser()==$user;
                // logic to determine if the user can VIEW
                // return true or false
                break;
        
        }

        return false;
    }
}
