<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('accueil/index.html.twig', [
            'controller_name' => 'AccueilController',
        ]);
    }
    /**
     * @Route("/messagerie", name="messagerie")
     */
    public function messagerie()
    {
        return $this->render('messagerie/index.html.twig', [
            'controller_name' => 'AccueilController',
        ]);
    }
}




    

