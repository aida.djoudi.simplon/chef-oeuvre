<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class DesinscriptionNewsletterInlController extends AbstractController
{

    /**
     * @Route("/desinscription", name="app_desinscription")
     */
    public function oubliPass(Request $request,UserRepository $userRepository, \Swift_Mailer $mailer): Response
        {
                    $user = $userRepository->findOneByEmail($request->query->get('email'));
                    //$email=$userRepository->getEmail();
                    $user->setNewsletter(false);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($user);
                    $entityManager->flush();
                    $this->addFlash('danger', 'vous êtes désabonnée de newsletter de site synchro et vous  pouvez vous réinscrire depuis votre compte');
                    return $this->redirectToRoute('accueil');             
        }
}
