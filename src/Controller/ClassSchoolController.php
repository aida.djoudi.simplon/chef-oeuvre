<?php

namespace App\Controller;

use App\Entity\ClassSchool;
use App\Form\ClassSchoolType;
use App\Repository\ClassSchoolRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/class")
 *  @IsGranted("ROLE_DIRECTEUR")
 */
class ClassSchoolController extends AbstractController
{
    /**
     * @Route("/", name="class_school_index", methods={"GET"})
     */
    public function index(ClassSchoolRepository $classSchoolRepository): Response
    {
        return $this->render('class_school/index.html.twig', [
            'class_schools' => $classSchoolRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="class_school_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $classSchool = new ClassSchool();
        $form = $this->createForm(ClassSchoolType::class, $classSchool);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($classSchool);
            $entityManager->flush();

            return $this->redirectToRoute('class_school_index');
        }

        return $this->render('class_school/new.html.twig', [
            'class_school' => $classSchool,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="class_school_show", methods={"GET"})
     */
    public function show(ClassSchool $classSchool): Response
    {
        return $this->render('class_school/show.html.twig', [
            'class_school' => $classSchool,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="class_school_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ClassSchool $classSchool): Response
    {
        $form = $this->createForm(ClassSchoolType::class, $classSchool);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('class_school_index');
        }

        return $this->render('class_school/edit.html.twig', [
            'class_school' => $classSchool,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="class_school_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ClassSchool $classSchool): Response
    {
        if ($this->isCsrfTokenValid('delete'.$classSchool->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($classSchool);
            $entityManager->flush();
        }

        return $this->redirectToRoute('class_school_index');
    }
}
