<?php

namespace App\Controller;

use App\Entity\ParentOfChild;
use App\Form\ParentOfChildType;
use App\Repository\ParentOfChildRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/parent/of/child")
 *  @IsGranted("ROLE_DIRECTEUR")
 */
class ParentOfChildController extends AbstractController
{
    /**
     * @Route("/", name="parent_of_child_index", methods={"GET"})
     */
    public function index(ParentOfChildRepository $parentOfChildRepository): Response
    {
        return $this->render('parent_of_child/index.html.twig', [
            'parent_of_children' => $parentOfChildRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="parent_of_child_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $parentOfChild = new ParentOfChild();
        $form = $this->createForm(ParentOfChildType::class, $parentOfChild);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parentOfChild);
            $entityManager->flush();

            return $this->redirectToRoute('parent_of_child_index');
        }

        return $this->render('parent_of_child/new.html.twig', [
            'parent_of_child' => $parentOfChild,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="parent_of_child_show", methods={"GET"})
     */
    public function show(ParentOfChild $parentOfChild): Response
    {
        return $this->render('parent_of_child/show.html.twig', [
            'parent_of_child' => $parentOfChild,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="parent_of_child_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ParentOfChild $parentOfChild): Response
    {
        $form = $this->createForm(ParentOfChildType::class, $parentOfChild);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('parent_of_child_index');
        }

        return $this->render('parent_of_child/edit.html.twig', [
            'parent_of_child' => $parentOfChild,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="parent_of_child_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ParentOfChild $parentOfChild): Response
    {
        if ($this->isCsrfTokenValid('delete'.$parentOfChild->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($parentOfChild);
            $entityManager->flush();
        }

        return $this->redirectToRoute('parent_of_child_index');
    }
}
