<?php

namespace App\Controller;
use App\Entity\Evenement;
use App\Repository\EvenementRepository;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EvenementListController extends AbstractController
{
    /**
     * @Route("/list/evenement", name="list-evenement")
     */
    public function liste(EvenementRepository $evenementRepository)
    {
        $value='Public';
        return $this->render('evenement/evenement-vu.html.twig', [
            'evenements' => $evenementRepository->findPublicEvent($value),
        ]);
    }
    /**
     * @Route("/evenement/description/{id}", name="evenement-detaille")
     */
    public function detaille(Evenement $evenement)
    {
        return $this->render('evenement/evenement-detail.html.twig', [
            'evenement' => $evenement,
        ]);
    }


}




