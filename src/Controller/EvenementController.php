<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Entity\SchoolTeam;
use App\Form\EvenementType;

use App\Repository\EvenementRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Notifications\EventClasseCreateNotification;
use App\Notifications\EventPublicCreateNotification;
use App\Notifications\EventServiceCreateNotification;
use App\Notifications\EventTeacherCreateNotification;
use App\Notifications\EventPersonelCreateNotification;
use App\Repository\SchoolTeamRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;


/**
 * @Route("/evenement")
 * 
 */
class EvenementController extends AbstractController
{


    /**
     * @Route("/", name="evenement_index", methods={"GET"})
     */
    public function index(EvenementRepository $evenementRepository)
    {
        $this->denyAccessUnlessGranted('ROLE_ENSEIGNANT');
       //////////////////test avec User//////////
        $user=$this->getUser();
       // dump($user);
       //recuperer les evenement de user conecter
        $events=$evenementRepository->findBy(array('user' =>$user)); 
       // dd($events);
        
        return $this->render('evenement/index.html.twig', [
            'evenements' => $events,
        ]);
    }
    /**
     * @Route("/atsem", name="evenement_index_atsem", methods={"GET"})
     */
    public function atsem(EvenementRepository $evenementRepository)
    {
        $this->denyAccessUnlessGranted('ROLE_ATSEM');
       //////////////////test avec User//////////
        $user=$this->getUser();
       // dump($user);
       //recuperer les evenement de user conecter
        $events=$evenementRepository->findBy(array('user' =>$user)); 
       // dd($events);
        
        return $this->render('evenement/index.html.twig', [
            'evenements' => $events,
        ]);
    }
    /**
     * @Route("/new", name="evenement_new", methods={"GET","POST"})
     */
    public function new(
        Request $request,
        EventServiceCreateNotification $serviceNotifier,
        EventPersonelCreateNotification $personelNotifier,
        EventClasseCreateNotification $classNotifier,
        EventPublicCreateNotification $publicNotifier,
        EventTeacherCreateNotification $enseignantNotifier
    ): Response
    {
        $evenement = new Evenement();
        $form = $this->createForm(EvenementType::class, $evenement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $evenement->setUser($this->getUser());
            $entityManager->persist($evenement);
            $entityManager->flush();
            ///a recupérer apres submit $value 
                $value = $evenement->getType();
              //  dd($value);
            switch ($value) {
                case "Public":
                    $publicNotifier->notify();
                break;
                
                case "Classe":
                    $classe=$request->request->get('classe');
                   // dd($classe);
                    $classNotifier->notify($classe);
                break;
                case "Enseignant(e)":
                    //recuperer le nom de l'enseignat
                    $id = $this->getUser()->getId();
                    //dd($id);
                    $enseignantNotifier->notify($id);
                break;
                case "Personnel":
                    $email=$request->request->get('perso');
                    $personelNotifier->notify($email);
                
                break; 
                case "Cantine":
                    $serviceNotifier->notify($value);
                
                break; 
                case "Garderie":
                    $serviceNotifier->notify($value);
                
                break; 
            }
            $rols = $this->getUser()->getRoles();
            foreach( $rols as $rol ){
                //dd($rol);
                if ($rol =='ROLE_DIRECTEUR'){
                    return $this->redirectToRoute('evenement_index');
                }
                if ($rol =='ROLE_ATSEM'){
                    return $this->redirectToRoute('evenement_index_atsem');
                }
            }
            
            
        }
        return $this->render('evenement/new.html.twig', [
            'evenement' => $evenement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenement_show", methods={"GET"})
     */
    public function show(Evenement $evenement): Response
    {
        $this->denyAccessUnlessGranted('view', $evenement);
        return $this->render('evenement/show.html.twig', [
            'evenement' => $evenement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="evenement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Evenement $evenement): Response
    {
        $this->denyAccessUnlessGranted('edit', $evenement);
        $form = $this->createForm(EvenementType::class, $evenement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $rols = $this->getUser()->getRoles();
            foreach( $rols as $rol ){
                //dd($rol);
                if ($rol =='ROLE_DIRECTEUR'){
                    return $this->redirectToRoute('evenement_index');
                }
                if ($rol =='ROLE_ATSEM'){
                    return $this->redirectToRoute('evenement_index_atsem');
                }
            }
        }

        return $this->render('evenement/edit.html.twig', [
            'evenement' => $evenement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Evenement $evenement): Response

    {

        $this->denyAccessUnlessGranted('delete', $evenement);
        if ($this->isCsrfTokenValid('delete'.$evenement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($evenement);
            $entityManager->flush();
        }

        $rols = $this->getUser()->getRoles();
        foreach( $rols as $rol ){
            //dd($rol);
            if ($rol =='ROLE_DIRECTEUR'){
                return $this->redirectToRoute('evenement_index');
            }
            if ($rol =='ROLE_ATSEM'){
                return $this->redirectToRoute('evenement_index_atsem');
            }
        }

    }
    
}
