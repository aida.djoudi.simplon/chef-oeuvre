<?php

namespace App\Controller;

use App\Entity\SchoolTeam;
use App\Form\SchoolTeamType;
use App\Repository\SchoolTeamRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/school/team")
 *  @IsGranted("ROLE_DIRECTEUR")
 */
class SchoolTeamController extends AbstractController
{
    /**
     * @Route("/", name="school_team_index", methods={"GET"})
     */
    public function index(SchoolTeamRepository $schoolTeamRepository): Response
    {
        return $this->render('school_team/index.html.twig', [
            'school_teams' => $schoolTeamRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="school_team_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $schoolTeam = new SchoolTeam();
        $form = $this->createForm(SchoolTeamType::class, $schoolTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($schoolTeam);
            $entityManager->flush();

            return $this->redirectToRoute('school_team_index');
        }

        return $this->render('school_team/new.html.twig', [
            'school_team' => $schoolTeam,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="school_team_show", methods={"GET"})
     */
    public function show(SchoolTeam $schoolTeam): Response
    {
        return $this->render('school_team/show.html.twig', [
            'school_team' => $schoolTeam,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="school_team_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SchoolTeam $schoolTeam): Response
    {
        $form = $this->createForm(SchoolTeamType::class, $schoolTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('school_team_index');
        }

        return $this->render('school_team/edit.html.twig', [
            'school_team' => $schoolTeam,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="school_team_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SchoolTeam $schoolTeam): Response
    {
        if ($this->isCsrfTokenValid('delete'.$schoolTeam->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($schoolTeam);
            $entityManager->flush();
        }

        return $this->redirectToRoute('school_team_index');
    }
}
