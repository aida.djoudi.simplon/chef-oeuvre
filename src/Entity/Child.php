<?php

namespace App\Entity;

use App\Repository\ChildRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ChildRepository::class)
 */
class Child
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOFBirth;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sex;

    /**
     * @ORM\ManyToOne(targetEntity=ClassSchool::class)
     * @Assert\NotBlank(message = "Champ obligatoire")
     */
    private $class;

    /**
     * @ORM\ManyToMany(targetEntity=Service::class, inversedBy="children")
     */
    private $service;

    /**
     * @ORM\ManyToMany(targetEntity=ParentOfChild::class, inversedBy="children",cascade={"persist"})
     */
    private $parent;

    /**
     * @ORM\ManyToMany(targetEntity=Evenement::class, inversedBy="children")
     */
    private $evenement;

    public function __construct()
    {
        $this->service = new ArrayCollection();
        $this->parent = new ArrayCollection();
        $this->evenement = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDateOFBirth(): ?\DateTimeInterface
    {
        return $this->dateOFBirth;
    }

    public function setDateOFBirth(\DateTimeInterface $dateOFBirth): self
    {
        $this->dateOFBirth = $dateOFBirth;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getClass(): ?ClassSchool
    {
        return $this->class;
    }

    public function setClass(?ClassSchool $class): self
    {
        $this->class = $class;

        return $this;
    }
    public function __toString(): string
    {
        return $this->firstName;
    }

    /**
     * @return Collection|Service[]
     */
    public function getService(): Collection
    {
        return $this->service;
    }

    public function addService(Service $service): self
    {
        if (!$this->service->contains($service)) {
            $this->service[] = $service;
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        if ($this->service->contains($service)) {
            $this->service->removeElement($service);
        }

        return $this;
    }

    /**
     * @return Collection|ParentOfChild[]
     */
    public function getParent(): Collection
    {
        return $this->parent;
    }

    public function addParent(ParentOfChild $parent): self
    {
        if (!$this->parent->contains($parent)) {
            $this->parent[] = $parent;
        }

        return $this;
    }

    public function removeParent(ParentOfChild $parent): self
    {
        if ($this->parent->contains($parent)) {
            $this->parent->removeElement($parent);
        }

        return $this;
    }

    /**
     * @return Collection|Evenement[]
     */
    public function getEvenement(): Collection
    {
        return $this->evenement;
    }

    public function addEvenement(Evenement $evenement): self
    {
        if (!$this->evenement->contains($evenement)) {
            $this->evenement[] = $evenement;
        }

        return $this;
    }

    public function removeEvenement(Evenement $evenement): self
    {
        if ($this->evenement->contains($evenement)) {
            $this->evenement->removeElement($evenement);
        }

        return $this;
    }


}
