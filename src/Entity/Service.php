<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ServiceRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ServiceRepository::class)
 * @UniqueEntity(fields={"name"}, message="le service est déja rajouter")
 */
class Service
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Child::class, mappedBy="service")
     */
    private $children;

    /**
     * @ORM\ManyToMany(targetEntity=SchoolTeam::class, mappedBy="service")
     */
    private $schoolTeams;

    /**
     * @ORM\ManyToMany(targetEntity=SchoolYear::class, inversedBy="services")
     */
    private $year;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->schoolTeams = new ArrayCollection();
        $this->year = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Child[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Child $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->addService($this);
        }

        return $this;
    }

    public function removeChild(Child $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            $child->removeService($this);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolTeam[]
     */
    public function getSchoolTeams(): Collection
    {
        return $this->schoolTeams;
    }

    public function addSchoolTeam(SchoolTeam $schoolTeam): self
    {
        if (!$this->schoolTeams->contains($schoolTeam)) {
            $this->schoolTeams[] = $schoolTeam;
            $schoolTeam->addService($this);
        }

        return $this;
    }

    public function removeSchoolTeam(SchoolTeam $schoolTeam): self
    {
        if ($this->schoolTeams->contains($schoolTeam)) {
            $this->schoolTeams->removeElement($schoolTeam);
            $schoolTeam->removeService($this);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolYear[]
     */
    public function getYear(): Collection
    {
        return $this->year;
    }

    public function addYear(SchoolYear $year): self
    {
        if (!$this->year->contains($year)) {
            $this->year[] = $year;
        }

        return $this;
    }

    public function removeYear(SchoolYear $year): self
    {
        if ($this->year->contains($year)) {
            $this->year->removeElement($year);
        }

        return $this;
    }
    public function __toString(): string
        {
            return $this->name;
        }
}

