<?php

namespace App\Entity;

use App\Repository\SchoolTeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SchoolTeamRepository::class)
 */
class SchoolTeam
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

    /**
     * @ORM\Column(type="integer")
     */
    private $phone;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="schoolTeam", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $post;


    /**
     * @ORM\ManyToMany(targetEntity=Service::class, inversedBy="schoolTeams")
     */
    private $service;

    /**
     * @ORM\ManyToMany(targetEntity=ClassSchool::class, inversedBy="schoolTeams",cascade={"persist"})
     */
    private $classOfSchool;

    public function __construct()
    {
        $this->class = new ArrayCollection();
        $this->service = new ArrayCollection();
        $this->classOfSchool = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
    public function __toString(): string
    {
        return $this->firstName;
    }

    public function getPost(): ?string
    {
        return $this->post;
    }

    public function setPost(string $post): self
    {
        $this->post = $post;

        return $this;
    }



    /**
     * @return Collection|Service[]
     */
    public function getService(): Collection
    {
        return $this->service;
    }

    public function addService(Service $service): self
    {
        if (!$this->service->contains($service)) {
            $this->service[] = $service;
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        if ($this->service->contains($service)) {
            $this->service->removeElement($service);
        }

        return $this;
    }

    /**
     * @return Collection|ClassSchool[]
     */
    public function getClassOfSchool(): Collection
    {
        return $this->classOfSchool;
    }

    public function addClassOfSchool(ClassSchool $classOfSchool): self
    {
        if (!$this->classOfSchool->contains($classOfSchool)) {
            $this->classOfSchool[] = $classOfSchool;
        }

        return $this;
    }

    public function removeClassOfSchool(ClassSchool $classOfSchool): self
    {
        if ($this->classOfSchool->contains($classOfSchool)) {
            $this->classOfSchool->removeElement($classOfSchool);
        }

        return $this;
    }


    
}
