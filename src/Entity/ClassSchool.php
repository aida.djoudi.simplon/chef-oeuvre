<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ClassSchoolRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ClassSchoolRepository::class)
 */
class ClassSchool
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $classSchool;

    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $schoolSupplies;



    /**
     * @ORM\ManyToMany(targetEntity=SchoolYear::class, mappedBy="class")
     */
    private $schoolYears;

    /**
     * @ORM\ManyToMany(targetEntity=SchoolTeam::class, mappedBy="classOfSchool")
     */
    private $schoolTeams;

    public function __construct()
    {
        $this->schoolTeams = new ArrayCollection();
        $this->schoolYears = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClassSchool(): ?string
    {
        return $this->classSchool;
    }

    public function setClassSchool(string $classSchool): self
    {
        $this->classSchool = $classSchool;

        return $this;
    }

    public function getSchoolSupplies(): ?string
    {
        return $this->schoolSupplies;
    }

    public function setSchoolSupplies(?string $schoolSupplies): self
    {
        $this->schoolSupplies = $schoolSupplies;

        return $this;
    }
    public function __toString(): string
    {
        return $this->classSchool;
    }

    /**
     * @return Collection|SchoolYear[]
     */
    public function getSchoolYears(): Collection
    {
        return $this->schoolYears;
    }

    public function addSchoolYear(SchoolYear $schoolYear): self
    {
        if (!$this->schoolYears->contains($schoolYear)) {
            $this->schoolYears[] = $schoolYear;
            $schoolYear->addClass($this);
        }

        return $this;
    }

    public function removeSchoolYear(SchoolYear $schoolYear): self
    {
        if ($this->schoolYears->contains($schoolYear)) {
            $this->schoolYears->removeElement($schoolYear);
            $schoolYear->removeClass($this);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolTeam[]
     */
    public function getSchoolTeams(): Collection
    {
        return $this->schoolTeams;
    }

    public function addSchoolTeam(SchoolTeam $schoolTeam): self
    {
        if (!$this->schoolTeams->contains($schoolTeam)) {
            $this->schoolTeams[] = $schoolTeam;
            $schoolTeam->addClassOfSchool($this);
        }

        return $this;
    }

    public function removeSchoolTeam(SchoolTeam $schoolTeam): self
    {
        if ($this->schoolTeams->contains($schoolTeam)) {
            $this->schoolTeams->removeElement($schoolTeam);
            $schoolTeam->removeClassOfSchool($this);
        }

        return $this;
    }
}
