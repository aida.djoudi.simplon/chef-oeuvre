<?php

namespace App\Entity;

use App\Repository\SchoolYearRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SchoolYearRepository::class)
 */
class SchoolYear
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $schoolYear;

    /**
     * @ORM\ManyToMany(targetEntity=Service::class, mappedBy="year")
     */
    private $services;

    /**
     * @ORM\ManyToMany(targetEntity=ClassSchool::class, inversedBy="schoolYears")
     */
    private $class;

    /**
     * @ORM\ManyToMany(targetEntity=Evenement::class, inversedBy="schoolYears")
     */
    private $evenement;

    public function __construct()
    {
        $this->services = new ArrayCollection();
        $this->class = new ArrayCollection();
        $this->evenement = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchoolYear(): ?int
    {
        return $this->schoolYear;
    }

    public function setSchoolYear(int $schoolYear): self
    {
        $this->schoolYear=$schoolYear;

        return $this;
    }

    /**
     * @return Collection|Service[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->addYear($this);
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        if ($this->services->contains($service)) {
            $this->services->removeElement($service);
            $service->removeYear($this);
        }

        return $this;
    }

    /**
     * @return Collection|ClassSchool[]
     */
    public function getClass(): Collection
    {
        return $this->class;
    }

    public function addClass(ClassSchool $class): self
    {
        if (!$this->class->contains($class)) {
            $this->class[] = $class;
        }

        return $this;
    }

    public function removeClass(ClassSchool $class): self
    {
        if ($this->class->contains($class)) {
            $this->class->removeElement($class);
        }

        return $this;
    }

    /**
     * @return Collection|Evenement[]
     */
    public function getEvenement(): Collection
    {
        return $this->evenement;
    }

    public function addEvenement(Evenement $evenement): self
    {
        if (!$this->evenement->contains($evenement)) {
            $this->evenement[] = $evenement;
        }

        return $this;
    }

    public function removeEvenement(Evenement $evenement): self
    {
        if ($this->evenement->contains($evenement)) {
            $this->evenement->removeElement($evenement);
        }

        return $this;
    }
    public function __toString(): string
        {
            return $this->schoolYear;
        }

}
