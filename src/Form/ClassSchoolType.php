<?php

namespace App\Form;

use App\Entity\ClassSchool;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ClassSchoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('classSchool'
            //ChoiceType::class, [
            //     'choices'  => [
            //         'TPS' => 'TPS',
            //         'PS'=>'PS',
            //         'MS'=>'MS',
            //         'GS'=>'GS',
            //         'CP'=>'CP',
            //         'CE1'=>'CE1',
            //         'CE2'=>'CE2',
            //         'CM1'=>'CM1',
            //         'CM2'=>'CM2',

            // ],
           // 'expanded' => false,
           // 'multiple' => false,
            //]
        )
            ->add('schoolSupplies')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ClassSchool::class,
        ]);
    }
}
