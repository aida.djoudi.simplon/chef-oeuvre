<?php

namespace App\Form;

use App\Entity\ParentOfChild;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class ParentOfChildType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Nom',
                
                'constraints' => [
                        new NotBlank([
                            'message' => 'Champ obligatoire ',
                        ]),
                    ]
        
                
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Prénom',
                
                'constraints' => [
                    new NotBlank([
                        'message' => 'Champ obligatoire',
                    ]),
                ]
            ])
            ->add('adress', TextType::class, [
                'label' => 'Adresse',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Champ obligatoire',
                    ]),
                ]
            ])
            ->add('phone', IntegerType::class, [
                'label' => 'Tél',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Champ obligatoire',
                    ]),
                    new Length([
                        'min' => 9,
                        'minMessage' => ' numéro de télephone ne doit pas dépasser {{ limit }} chifres',
                        // max length allowed by Symfony for security reasons
                        'max' => 9 ,
                    ]),
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'constraints' => [
                    new Email([
                        'message' => ' {{ value }} n\'est pas valide.',
                    ]),
                    new NotBlank([
                        'message' => 'Champ obligatoire',
                    ]),
                ]
            ])
        ;
        // $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($options) {
        //     // ... adding the name field if needed
        //     $data = $event->getData();
        //     if(!$data)
        //     return;
        //     $child = $options['child'];
        //     //dd($child);
        //     $data->addChild($options['child']);
        // });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ParentOfChild::class,
            'child' => null
        ]);
    }
}
