<?php

namespace App\Form;

use App\Entity\Evenement;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Security\Core\Security;

class EvenementType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        // Avoid calling getUser() in the constructor: auth may not
        // be complete yet. Instead, store the entire Security object.
        $this->security = $security;
    
    
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('abstract', TextareaType::class)
            ->add('description', CKEditorType::class)
            ->add('imageFile', VichImageType::class, [
                
                'allow_delete' => true,
                'delete_label' => 'Supprimer',
                'download_label' => 'Télécharger',
                'download_uri' => true,
                'image_uri' => true,
                'asset_helper' => true,
            ])
            // ->add('fileName',VichFileType::class, [
            
            //     'allow_delete' => true,
            //     'delete_label' => 'Supprimer',
            //     'download_uri' => true,
            //     'download_label' => 'Télécharger',
            //     'asset_helper' => true,
            
            // ])
            ->add('startDate',DateType::class, [
                
                'format' => 'dd MM yyyy',
                'years' => range(date('2020'), date('Y')+5),
            ])
            ->add('endDate',DateType::class, [
                
                'format' => 'dd MM yyyy',
                'years' => range(date('2020'), date('Y')+5),
                
            ])

            
            ->add('type', ChoiceType::class, [
                'choices'  => $this->choiceType(),
            'expanded' => true,
            'required' => true,
            'multiple' => false,
            'choice_attr' => function($choice, $key, $value){
                return ['class' => 'js-type'];
            }
        ]
        )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evenement::class,
        ]);
    }

    private function choiceType(){
        $rols = $this->security->getUser()->getRoles();
        //dd($rols);
        foreach( $rols as $rol ){
            //dd($rol);
            if ($rol =='ROLE_DIRECTEUR'){
                $var=[
                'Public' => 'Public',
                'Personnel' => 'Personnel',
                'Classe'=>'Classe',
                // 'Cantine'=>'Cantine',
                // 'Garderie'=>'Garderie',
                // 'Etude dirigée'=>'Etude dirigée',
                ];
            }
            if ($rol =='ROLE_ENSEIGNANT'){
                $var=[
                // 'Classe'=>'Classe',
                // 'Etude dirigée'=>'Etude dirigée',
                'Enseignant(e)'=>'Enseignant(e)',
                'Personnel' => 'Personnel'
                
                ];
            }
            if ($rol =='ROLE_ATSEM'){
                $var=[
                    'Cantine'=>'Cantine',
                    'Garderie'=>'Garderie',
                
                ];
            }
            if ($rol =='ROLE_SERVICE'){
                $var=[
                    'Cantine'=>'Cantine',
                    'Garderie'=>'Garderie',
                
                ];
            }
            return $var;
        } 
    
    }
}
