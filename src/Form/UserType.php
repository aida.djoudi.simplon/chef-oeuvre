<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            // ->add('lastName')
            // ->add('firstName')
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Directeur' => 'ROLE_DIRECTEUR',
                    'Enseignat(e)' => 'ROLE_ENSEIGNANT',
                    'Atsem'=>'ROLE_ATSEM',
                    // 'Service'=>'ROLE_SERVICE'
                    

                ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles' 
            ])
            
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
