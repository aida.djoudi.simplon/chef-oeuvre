<?php

namespace App\Form;

use App\Entity\SchoolTeam;
use App\Entity\ClassSchool;
use App\Form\ClassSchoolType;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class SchoolTeamType extends AbstractType
{

    public function __construct( UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('firstName', TextType::class, [
            'label' => 'Nom',
            
            'constraints' => [
                    new NotBlank([
                        'message' => 'Champ obligatoire ',
                    ]),
                ]
    
            
        ])
        ->add('lastName', TextType::class, [
            'label' => 'Prénom',
            
            'constraints' => [
                new NotBlank([
                    'message' => 'Champ obligatoire',
                ]),
            ]
        ])
        ->add('adress', TextType::class, [
            'label' => 'Adresse',
            'constraints' => [
                new NotBlank([
                    'message' => 'Champ obligatoire',
                ]),
            ]
        ])
            ->add('phone', TelType::class, [
                'label' => 'Tél',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Champ obligatoire',
                    ]),
                ]
                ]

                )
            ->add('user',ChoiceType::class, [
                'choices'  => $this->choiceUser() ]
                ) 
            ->add('post',
            ChoiceType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Champ obligatoire',
                    ]),
                    ],
                    'choices'  => [
                        'Directeur' => 'Directeur',
                        'Enseignant' => 'Enseignant(e)',
                        'Atsem' => 'Atsem',
                ],
                'expanded' => true,
                'multiple' => false,
            
            ]
            
            )
            ->add('classOfSchool')
            ->add('service')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SchoolTeam::class,
        ]);
    }

    private function choiceUser(){
        $value='grenoble';
        $users = $this->userRepository->findEquipeUser($value);
        $emails=[];
        foreach ($users as $user) {
            $emails[$user->getEmail()]=$user;
        }
        return $emails;
        
    
    }
}
