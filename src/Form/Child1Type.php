<?php

namespace App\Form;

use App\Entity\Child;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class Child1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $child = $builder->getData();
        $builder
        ->add('firstName', TextType::class, [
            'label' => 'Nom',
            
            'constraints' => [
                    new NotBlank([
                        'message' => 'Champ obligatoire ',
                    ]),
                ]
    
            
        ])
        ->add('lastName', TextType::class, [
            'label' => 'Prénom',
            
            'constraints' => [
                new NotBlank([
                    'message' => 'Champ obligatoire',
                ]),
            ]
        ])
            ->add('dateOFBirth',BirthdayType::class, [
                
                'format' => 'dd MM yyyy',
                // 'days' => range(1,31),
                // 'years'=> range(2016,2030),
                'years' => range(date('2009'), date('Y')),
                'constraints' => [
                    new NotBlank([
                        'message' => 'Champ obligatoire',
                    ]),
                ]
            ])
            ->add('sex',
            ChoiceType::class, [
                    'choices'  => [
                        'Masculin' => 'Masculin',
                        'Féminin' => 'Féminin',
                ],
                    'expanded' => true,
                    'multiple' => false,
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Champ obligatoire',
                        ]),
                    ]
            ]
            
            )
            ->add('service')
            ->add('class'
            
            )
            
            ->add('parent', CollectionType::class, [
                'entry_type' => ParentOfChildType::class,
                'entry_options' => [
                    'label' => false,
                    'child' => $child
                ],
                'allow_add' => true,
                // 'allow_delete' => true,
                'by_reference'=>true,    
            ])

            // ->add('enregistrer',SubmitType::class,[
            //     'attr'=>[
            //         'class'=>'btn btnColor'
            //     ]
            // ])
                
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Child::class,
        ]);
    }
}
