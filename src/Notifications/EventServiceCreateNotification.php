<?php

namespace App\Notifications;

use Swift_Message;
use Twig\Environment;
use App\Entity\Evenement;
use App\Form\EvenementType;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\Error\RuntimeError;
use Symfony\Component\Mime\Email;
use App\Repository\UserRepository;
use App\Repository\ChildRepository;
use App\Repository\ServiceRepository;
use App\Repository\EvenementRepository;
use App\Repository\ParentOfChildRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EventServiceCreateNotification
{
    
    /**
     * Propriété contenant le module d'envoi de mail
     * 
     * @var \MailerInterface
     */
    private $mailer;

    /**
     * Propriété contenant l'environnement twig
     * 
     * @var Environment
     */
    private $renderer;


    /**
     * Constructeur de classe
     * @param MailerInterface $mailer
     * @param Environment $renderer
     */
    
    public function __construct(
        MailerInterface $mailer, 
        Environment $renderer,
        EvenementRepository $evenementRepository,
        ServiceRepository $serviceRepository,
        ChildRepository $childRepository,
        ParentOfChildRepository $parentOfChildRepository
        
        )
    {
        $this->mailer                   = $mailer;
        $this->renderer                 = $renderer;
        $this->evenementRepository      = $evenementRepository;
        $this->serviceRepository        = $serviceRepository;
        $this->childRepository          = $childRepository;
        $this->parentOfChildRepository  = $parentOfChildRepository;
        
        // $evenement = new Evenement();
        // $form = $this->createForm(EvenementType::class, $evenement);
        // $form->handleRequest($request);
        
    }

    /**
     * Méthode de notification (envoi de mail)
     * 
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notify($value)
    {
            //dd($value);
            //1.recupérer  id de  service qui sont le mm nom que value 
                $services = $this->serviceRepository;
                $services = $services->findService($value);
                foreach ($services as $service) {
                    $idService=$service->getId();
                    //dd($service);
                    //dd($idService);
                    
            //2.récupérer les id enfants quant service.id =$idService 
                    $childs = $this->childRepository;
                    $childs = $childs->findChildWithService($idService);
                    //dd( $childs);
                    $childsFIds= [];
                        foreach($childs as $child){
                        $childsFIds[]=$child->getId();
                        }
                    // dd($childsFIds);
            //3.récupérer les email parents 
                            $emails= [];
                            foreach($childsFIds as $childsFId){
                                // dd($childsFIds);
                                $parents=$this->parentOfChildRepository;
                                $parents=$parents->findParent($childsFId);
                                // $parents=[];
                                foreach($parents as $parent){
                                        $emails[] =$parent->getEmail();
                                }
                            }
                }
           // dd($emails);
           // $value='Cantine';
            $evenements = $this->evenementRepository;
            $evenements = $evenements->findLastEvent();    
            //dd($actualite);
                foreach ($evenements as $evenement) {
                    $type=$evenement->getType();
                    if($type==$value){
                        $titre=$evenement->getTitle();
                        $id=$evenement->getId();
                        $description=$evenement->getDescription();
                        //dd($emails);
                        foreach($emails as $email){
                            $message = new Email();
                            $message->from('aida.djoudi.simplon@gmail.com')
                            ->To($email)
                            ->subject('Ecole-Biollay/'.$type)
                            ->priority(Email::PRIORITY_HIGH)
                            ->html( 
                                $this->renderer->render(
                                    'emails/evenement-service.html.twig',['titre'=>$titre,'description'=>$description,'type'=>$type]
                                    ),
                                        'text/html'
                                    );
                                    ;
                                    $this->mailer->send($message);
                        }
                    }
                
            }
    
        
    }
}