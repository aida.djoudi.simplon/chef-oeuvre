<?php

namespace App\Notifications;

use Swift_Message;
use Twig\Environment;
use App\Entity\Evenement;
use App\Form\EvenementType;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\Error\RuntimeError;
use Symfony\Component\Mime\Email;
use App\Repository\UserRepository;
use App\Repository\ChildRepository;
use App\Repository\ServiceRepository;
use App\Repository\EvenementRepository;
use App\Repository\ClassSchoolRepository;
use App\Repository\ParentOfChildRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EventClasseCreateNotification
{
    
    /**
     * Propriété contenant le module d'envoi de mail
     * 
     * @var \MailerInterface
     */
    private $mailer;

    /**
     * Propriété contenant l'environnement twig
     * 
     * @var Environment
     */
    private $renderer;


    /**
     * Constructeur de classe
     * @param MailerInterfacer $mailer
     * @param Environment $renderer
     */
    
    public function __construct(
        MailerInterface $mailer, 
        Environment $renderer,
        EvenementRepository $evenementRepository,
        ServiceRepository $serviceRepository,
        ChildRepository $childRepository,
        ParentOfChildRepository $parentOfChildRepository,
        ClassSchoolRepository $classSchoolRepository
        
        )
    {
        $this->mailer                   = $mailer;
        $this->renderer                 = $renderer;
        $this->evenementRepository      = $evenementRepository;
        $this->serviceRepository        = $serviceRepository;
        $this->childRepository          = $childRepository;
        $this->parentOfChildRepository  = $parentOfChildRepository;
        $this->classSchoolRepository    = $classSchoolRepository;
        
        // $evenement = new Evenement();
        // $form = $this->createForm(EvenementType::class, $evenement);
        // $form->handleRequest($request);
        
    }

    /**
     * Méthode de notification (envoi de mail)
     * 
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notify($classe)
    {
        
            //1.recupérer tous les id de  classe qui sont le mm nom que value 
                $classes = $this->classSchoolRepository;
                $classes = $classes->findClasse($classe);
                foreach ($classes as $classe) {
                    $idClasse=$classe->getId();
                   // dd($idClasse);
                    //dd($idService);
                    
            //2.récupérer les id enfant quant Class_id =$idClasse 
                    $childs = $this->childRepository;
                   // dd( $childs);
                    $childs = $childs->findChildWithClasse($idClasse);
                   // dd( $childs);
                    $childsFIds= [];
                        foreach($childs as $child){
                        // $idchild=$child->getId();
                        // dd( $idchild);
                        $childsFIds[]=$child->getId();
                        }
                    //dd($childsFIds);
            //3.récupérer les email parents 
                            $emails= [];
                            foreach($childsFIds as $childsFId){
                                // dd($childsFIds);
                                $parents=$this->parentOfChildRepository;
                                $parents=$parents->findParent($childsFId);
                                // $parents=[];
                                foreach($parents as $parent){
                                        $emails[] =$parent->getEmail();
                                }
                            }
                }
            //dd($emails);
            $evenements = $this->evenementRepository;
            $evenements = $evenements->findLastEvent();    
            //dd($actualite);
                foreach ($evenements as $evenement) {
                    $type=$evenement->getType();
                        $titre=$evenement->getTitle();
                        $id=$evenement->getId();
                        $description=$evenement->getDescription();
                        //dd($emails);
                        foreach($emails as $email){
                            $message = new Email();
                            $message->from('aida.djoudi.simplon@gmail.com')
                            ->To($email)
                            ->subject('Ecole-Biollay/'.$classe)
                            ->priority(Email::PRIORITY_HIGH)
                            ->html(
                                $this->renderer->render(
                                            'emails/evenement-classe.html.twig',['titre'=>$titre,'description'=>$description,'classe'=>$classe]
                                        ),
                                            'text/html'
                                            );
                    ;

                           // $message = (new \Swift_Message('Ecole-Biollay/'.$classe))
                             //   ->setFrom('aida.djoudi.simplon@gmail.com')
                              //  ->setTo($email)
                              //  ->setBody(
                                    // $titre.'&nbsp; dans les actualité est rajouté <br><a href="https://127.0.0.1:8000/actualites/'.$id.'">Cliquer ici pour le voir</a>
                                    // <br><a href="https://127.0.0.1:8000/desinscription?email='.$email.'">Se désabonnée</a>', 
                               //     $this->renderer->render(
                              //      'emails/evenement-classe.html.twig',['titre'=>$titre,'description'=>$description,'classe'=>$classe]
                              //  ),
                                //    'text/html'
                                     //pour dev on utilise https://127.0.0.1:8000
                             //   );
                            // $message->attach(\Swift_Attachment::fromPath($this->get('kernel')->getRootDir()."/../web/uploads/file/Djoudi_Aida_CV.pdf"));

                            //$mailer->send($message);
                            $this->mailer->send($message);
                        }
                    }
                
            }
    
        
    
}