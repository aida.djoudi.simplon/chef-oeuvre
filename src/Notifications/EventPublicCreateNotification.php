<?php

namespace App\Notifications;

use Swift_Message;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\Error\RuntimeError;
use App\Repository\UserRepository;
use App\Repository\EvenementRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Mime\Email;

class EventPublicCreateNotification
{
    
    /**
     * Propriété contenant le module d'envoi de mail
     * 
     * @var \MailerInterface
     */
    private $mailer;

    /**
     * Propriété contenant l'environnement twig
     * 
     * @var Environment
     */
    private $renderer;

    /**
     * Constructeur de classe
     * @param MailerInterface $mailer
     * @param Environment $renderer
     */
    
    public function __construct(MailerInterface $mailer ,
        Environment $renderer,
        EvenementRepository $evenementRepository,
        UserRepository $userRepository)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
        $this->evenementRepository = $evenementRepository;
        $this->userRepository = $userRepository;
        
        
    }

    /**
     * Méthode de notification (envoi de mail)
     * 
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notify()

    {
        //////////////////////////envoie email////////////////////////////////////
    
        $emails = [];
        $users = $this->userRepository;
        $users = $users->findActivateNewsletter();
        //dd($users);
        foreach ($users as $user) {
            $emails[$user->getEmail()] = $user->getEmail();
            
        }
        //dd($emails);
        $value='Public';
        $evenements = $this->evenementRepository;
        $evenements = $evenements->findLastEvent();    
       // dd($evenements);
        foreach ($evenements as $evenement) {
            $type=$evenement->getType();
        //dd($emails);
            if($type==$value){
                $titre=$evenement->getTitle();
                $id=$evenement->getId();
                $description=$evenement->getDescription();
                foreach($emails as $email){
                    $message = new Email();
                   // $message = (new \Swift_Message('Ecole-Biollay/Un nouvelle evenement dans public est ajouté'))
                        $message->from('aida.djoudi.simplon@gmail.com')
                        ->To($email)
                        ->subject('Ecole-Biollay/Un nouvelle evenement dans public est ajouté')
                        ->priority(Email::PRIORITY_HIGH)
                        ->html(
                           // $titre.'&nbsp; dans les actualité est rajouté <br><a href="https://127.0.0.1:8000/actualites/'.$id.'">Cliquer ici pour le voir</a>
                           // <br><a href="https://127.0.0.1:8000/desinscription?email='.$email.'">Se désabonnée</a>', 
                            $this->renderer->render(
                            'emails/evenement-public.html.twig',['titre'=>$titre,'description'=>$description,'id' =>$id,'email'=>$email]
                        ),
                            'text/html'
                            //pour dev on utilise https://127.0.0.1:8000
                            //pour le site en ligne https://tous-synchro.synchro-bus.fr
                        );
                    ;
                    //$mailer->send($message);
                    $this->mailer->send($message);
                }
            }
            
        }
        
    }
}