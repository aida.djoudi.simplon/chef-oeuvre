<?php

namespace App\Notifications;

use Swift_Message;
use Twig\Environment;
use App\Entity\Evenement;
use App\Entity\SchoolTeam;
use App\Form\EvenementType;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\Error\RuntimeError;
use Symfony\Component\Mime\Email;
use App\Repository\ChildRepository;
use App\Repository\ServiceRepository;
use App\Repository\EvenementRepository;
use App\Repository\SchoolTeamRepository;
use App\Repository\ClassSchoolRepository;
use App\Repository\ParentOfChildRepository;
use Symfony\Component\Mailer\MailerInterface;

class EventTeacherCreateNotification
{
    
    /**
     * Propriété contenant le module d'envoi de mail
     * 
     * @var \MailerInterface
     */
    private $mailer;

    /**
     * Propriété contenant l'environnement twig
     * 
     * @var Environment
     */
    private $renderer;


    /**
     * Constructeur de classe
     * @param MailerInterface $mailer
     * @param Environment $renderer
     */
    
    public function __construct(
        MailerInterface $mailer, 
        Environment $renderer,
        EvenementRepository $evenementRepository,
        ServiceRepository $serviceRepository,
        ChildRepository $childRepository,
        ParentOfChildRepository $parentOfChildRepository,
        ClassSchoolRepository $classSchoolRepository,
        SchoolTeamRepository $schoolTeamRepository
        
        )
    {
        $this->mailer                   = $mailer;
        $this->renderer                 = $renderer;
        $this->evenementRepository      = $evenementRepository;
        $this->serviceRepository        = $serviceRepository;
        $this->childRepository          = $childRepository;
        $this->parentOfChildRepository  = $parentOfChildRepository;
        $this->classSchoolRepository    = $classSchoolRepository;
        $this->schoolTeamRepository    = $schoolTeamRepository;
        
    }

    /**
     * Méthode de notification (envoi de mail)
     * 
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notify($id)
    {
        //dd($id);
            //1.recupérer tous le id de  enseignat qui sont le mm nom et prenom que value 
                $teachers = $this->schoolTeamRepository->findTeacher($id);
               // dd($teachers);
                foreach ($teachers as $teacher) {
                    $idTeacher=$teacher->getId();
                    $enseignantFirstName=$teacher->getFirstName();
                    $enseignantLastName=$teacher->getLastName();
                }
               // dd($idTeacher);
            
            //2.recupérer tous les id de  classe qui sont le mm nom que value 
                $classes = $this->classSchoolRepository;
                $classes = $classes->findClasseWithTeacher($idTeacher);
                //dd($classes);
                $idClasses=[];
                foreach ($classes as $classe) {
                    $idClasses[]=$classe->getId();
                    //dd($idClasse);
                    
                } 
               // dd($idClasses);
               // 3.récupérer les id enfant quant Class_id =$idClasse 
                $childsId=[];
                                foreach($idClasses as $idClasse){
                                    // dd($childsFIds);
                                    $childs=$this->childRepository;
                                    $childs=$childs->findChildWithClasse($idClasse);
                                 //   dd($childs);
                                    
                                    foreach($childs as $child){
                                            $childsId[] =$child->getId();
                                    }
                                }
                           //     dd($childsId);
            //4.récupérer les email parents 
            $emails= [];
                            foreach($childsId as $childId){
                                // dd($childsFIds);
                                $parents=$this->parentOfChildRepository;
                                $parents=$parents->findParent($childId);
                                //dd($parents);
                                //$parents=[];
                                foreach($parents as $parent){
                                        $emails[] =$parent->getEmail();
                                }
                            }
                
            
           // dd($emails);

                    
            
            $evenements = $this->evenementRepository;
            $evenements = $evenements->findLastEvent();
            // //dd($evenements) ;   
                foreach ($evenements as $evenement) {
                    
                        $titre=$evenement->getTitle();
                        
                        $description=$evenement->getDescription();
                        
                //dd($emails);
                        foreach($emails as $email){
                            $message = new Email();
                            $message->from('aida.djoudi.simplon@gmail.com')
                            ->To($email)
                            ->subject('Ecole-Biollay/')
                            ->priority(Email::PRIORITY_HIGH)
                            ->html( 
                                $this->renderer->render(
                                    'emails/evenement-teacher.html.twig',['titre'=>$titre,'description'=>$description,'firstName'=>$enseignantFirstName,'lastName'=>$enseignantLastName]
                                    ),
                                        'text/html'
                                    );
                                    ;
                                    $this->mailer->send($message);

                            
                            // $message = (new \Swift_Message('Ecole-Biollay/'))
                            //     ->setFrom('aida.djoudi.simplon@gmail.com')
                            //     ->setTo($email)
                            //     ->setBody(
                            //         $this->renderer->render(
                            //         'emails/evenement-teacher.html.twig',['titre'=>$titre,'description'=>$description,'firstName'=>$enseignantFirstName,'lastName'=>$enseignantLastName]
                            //     ),
                            //         'text/html'
                                    
                            //     );
                            // // $message->attach(\Swift_Attachment::fromPath($this->get('kernel')->getRootDir()."/../web/uploads/file/Djoudi_Aida_CV.pdf"));
                            // $this->mailer->send($message);
                            
                        }
                }
    }
} 
