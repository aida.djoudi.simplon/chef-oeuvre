<?php

namespace App\Notifications;

use Swift_Message;
use Twig\Environment;
use App\Entity\Evenement;
use App\Form\EvenementType;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\Error\RuntimeError;
use Symfony\Component\Mime\Email;
use App\Repository\UserRepository;
use App\Repository\ChildRepository;
use App\Repository\ServiceRepository;
use App\Repository\EvenementRepository;
use App\Repository\ParentOfChildRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EventPersonelCreateNotification
{
    
    /**
     * Propriété contenant le module d'envoi de mail
     * 
     * @var \MailerInterface
     */
    private $mailer;

    /**
     * Propriété contenant l'environnement twig
     * 
     * @var Environment
     */
    private $renderer;


    /**
     * Constructeur de classe
     * @param MailerInterface $mailer
     * @param Environment $renderer
     */
    
    public function __construct(
        MailerInterface $mailer, 
        Environment $renderer,
        EvenementRepository $evenementRepository
    )
    
    {
        $this->mailer                   = $mailer;
        $this->renderer                 = $renderer;
        $this->evenementRepository      = $evenementRepository;
    
    }

    /**
     * Méthode de notification (envoi de mail)
     * 
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notify($email)
    {
            //ici je pourrais meme passer directement evenement on argument avec la method notify depui le sontroller                
            $evenements = $this->evenementRepository;
            $evenements = $evenements->findLastEvent();    
            //dd($actualite);
                foreach ($evenements as $evenement) {
                        $titre=$evenement->getTitle();
                        $description=$evenement->getDescription();
                        $message = new Email();
                            $message->from('aida.djoudi.simplon@gmail.com')
                            ->To($email)
                            ->subject('Ecole-Biollay')
                            ->priority(Email::PRIORITY_HIGH)
                            ->html(
                                $this->renderer->render(
                                    'emails/evenement-personel.html.twig',['titre'=>$titre,'description'=>$description,'email'=>$email]
                                        ),
                                            'text/html'
                                            );
                                            $this->mailer->send($message);
                                        }
                        
                            // $message = (new \Swift_Message('Ecole-Biollay'))
                            //     ->setFrom('aida.djoudi.simplon@gmail.com')
                            //     ->setTo($email)
                            //     ->setBody(
                            //         // $titre.'&nbsp; dans les actualité est rajouté <br><a href="https://127.0.0.1:8000/actualites/'.$id.'">Cliquer ici pour le voir</a>
                            //         // <br><a href="https://127.0.0.1:8000/desinscription?email='.$email.'">Se désabonnée</a>', 
                            //         $this->renderer->render(
                            //         'emails/evenement-personel.html.twig',['titre'=>$titre,'description'=>$description,'email'=>$email]
                            //     ),
                            //         'text/html'
                            //          //pour dev on utilise https://127.0.0.1:8000
                            //     );
                            
                            // $message->attach(\Swift_Attachment::fromPath($this->get('kernel')->getRootDir()."/../web/uploads/file/Djoudi_Aida_CV.pdf"));
                            //$mailer->send($message);
                        
    }
}